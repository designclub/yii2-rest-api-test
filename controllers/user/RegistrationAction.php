<?php

namespace app\controllers\user;

use Yii;
use yii\base\Action;
use app\models\RegistrationForm;

class RegistrationAction extends Action
{
    public function run()
    {
        $model = new RegistrationForm();
        $model->load(Yii::$app->request->bodyParams, '');
        $model->register();

        return $model;
    }
}