<?php

namespace app\controllers\user;

use yii\base\Action;
use app\models\User;
use app\models\Token;

class EmailConfirmAction extends Action
{
    /**
     * @throws CHttpException
     */
    public function run($token)
    {
        $tokenModel = Token::find()
            ->andWhere(['token' => $token, 'type' => Token::CONFIRM])
            ->andWhere(['>', 'expire', time()])->one();

        if (!$tokenModel) {
            throw new \yii\web\HttpException(404, 'Token could not be found.');
        }

        $tokenModel->user->status = User::STATUS_ACTIVE;
        $tokenModel->user->save();

        return true;
    }
}