<?php

namespace app\controllers\user;

use Yii;
use yii\base\Action;
use app\models\Token;

class ResetPasswordConfirmAction extends Action
{
    /**
     * @throws CHttpException
     */
    public function run($token)
    {
        $tokenModel = Token::find()
            ->andWhere(['token' => $token, 'type' => Token::CHANGE_PASSWORD])
            ->andWhere('expire > '.time())->one();

        if (!$tokenModel) {
            throw new \yii\web\HttpException(404, 'Token could not be found.');
        }

        return true;
    }
}