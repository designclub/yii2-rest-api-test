<?php

namespace app\controllers\user;

use Yii;
use yii\base\Action;
use app\models\ResetPasswordEmailForm;

class ResetPasswordEmailAction extends Action
{
    /**
     * @throws CHttpException
     */
    public function run()
    {
        $model = new ResetPasswordEmailForm();
        $model->load(Yii::$app->request->bodyParams, '');
        if ($token = $model->accessChange()) {
            return $token;
        }

        return $model;
    }
}