<?php

namespace app\controllers\user;

use Yii;
use yii\base\Action;
use app\models\LoginForm;
use yii\web\HttpException;

class LoginAction extends Action
{
    /**
     * @throws HttpException
     */
    public function run()
    {
        $model = new LoginForm();
        $model->load(Yii::$app->request->bodyParams, '');
        if ($token = $model->login()) {
            return $token;
        }

        return $model;
    }
}