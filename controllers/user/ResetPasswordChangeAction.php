<?php

namespace app\controllers\user;

use Yii;
use yii\base\Action;
use app\models\ResetPasswordChangeForm;

class ResetPasswordChangeAction extends Action
{
    /**
     * @throws CHttpException
     */
    public function run()
    {
        $model = new ResetPasswordChangeForm();
        $model->load(Yii::$app->request->bodyParams, '');
        if ($token = $model->changePassword()) {
            return $token;
        }

        return $model;
    }
}