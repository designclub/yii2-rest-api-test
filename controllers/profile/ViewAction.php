<?php

namespace app\controllers\profile;

use Yii;
use yii\base\Action;
use app\models\Profile;

class ViewAction extends Action
{
    public function run()
    {
        return Yii::$app->user->identity;
    }
}