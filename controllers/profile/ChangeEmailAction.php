<?php

namespace app\controllers\profile;

use Yii;
use yii\base\Action;
use app\models\Token;
use app\models\User;
use app\models\ChangeEmailForm;

class ChangeEmailAction extends Action
{
    public function run()
    {
        $model = new ChangeEmailForm();
        $model->load(Yii::$app->request->bodyParams, '');
        
        if ($model->validate()) {
            $user = User::findOne(['id' => Yii::$app->user->id]);
            $user->setAttributes(Yii::$app->request->bodyParams);
            $user->save();

            $token = new Token();
            $token->user_id = Yii::$app->user->id;
            $token->type = Token::CONFIRM;
            $token->generateToken(time() + 3600 * 24);
            $token->save();
            // Здесь отправка токена на почту

            return Yii::$app->user->identity;
        }
        
        return $model;
    }
}