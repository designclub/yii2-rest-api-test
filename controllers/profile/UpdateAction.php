<?php

namespace app\controllers\profile;

use Yii;
use yii\base\Action;

class UpdateAction extends Action
{
    public function run()
    {
        $model = Yii::$app->user->profile;
        $model->setAttributes(Yii::$app->request->bodyParams);
        $model->save();

        return $model;
    }
}