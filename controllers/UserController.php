<?php

namespace app\controllers;

use Yii;

class UserController extends \yii\rest\Controller
{
    public function actions()
    {
        return [
            'email-confirm' => [
                'class' => 'app\controllers\user\EmailConfirmAction'
            ],
            'login' => [
                'class' => 'app\controllers\user\LoginAction'
            ],
            'registration' => [
                'class' => 'app\controllers\user\RegistrationAction'
            ],
            'reset-password-change' => [
                'class' => 'app\controllers\user\ResetPasswordChangeAction'
            ],
            'reset-password-confirm' => [
                'class' => 'app\controllers\user\ResetPasswordConfirmAction'
            ],
            'reset-password-email' => [
                'class' => 'app\controllers\user\ResetPasswordEmailAction'
            ],
        ];        
    }
}
