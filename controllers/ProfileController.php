<?php

namespace app\controllers;

use Yii;
use yii\filters\auth\HttpBearerAuth;

class ProfileController extends \yii\rest\Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator']['authMethods'] = [
            HttpBearerAuth::class
        ];

        return $behaviors;
    }

    public function actions()
    {
        return [
            'change-email' => [
                'class' => 'app\controllers\profile\ChangeEmailAction'
            ],
            'update' => [
                'class' => 'app\controllers\profile\UpdateAction'
            ],
            'view' => [
                'class' => 'app\controllers\profile\ViewAction'
            ],
        ];
    }
}
