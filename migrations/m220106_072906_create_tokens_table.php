<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tokens}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m220106_072906_create_tokens_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tokens}}', [
            'id' => $this->primaryKey(),
            'token' => $this->string()->comment('Токен'),
            'expire' => $this->integer()->comment('Дата окончания'),
            'type' => $this->integer()->comment('Тип токена'),
            'user_id' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-tokens-user_id}}',
            '{{%tokens}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-tokens-user_id}}',
            '{{%tokens}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-tokens-user_id}}',
            '{{%tokens}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-tokens-user_id}}',
            '{{%tokens}}'
        );

        $this->dropTable('{{%tokens}}');
    }
}
