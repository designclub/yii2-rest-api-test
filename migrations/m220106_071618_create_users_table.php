<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m220106_071618_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'create_at' => $this->integer()->comment('Дата создания'),
            'update_at' => $this->integer()->comment('Дата обновления'),
            'last_visit' => $this->integer()->comment('Последний визит'),
            'email' => $this->string()->comment('email'),
            'password' => $this->string()->comment('Пароль'),
            'status' => $this->integer()->comment('Статус'),
            'role' => $this->integer()->comment('Роль'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
