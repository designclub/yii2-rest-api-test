<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%profiles}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m220106_071826_create_profiles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%profiles}}', [
            'id' => $this->primaryKey(),
            'phone' => $this->string()->comment('Телефон'),
            'first_name' => $this->string()->comment('Имя'),
            'last_name' => $this->string()->comment('Фамилия'),
            'middle_name' => $this->string()->comment('Отчество'),
            'address' => $this->string()->comment('Адрес'),
            'user_id' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-profiles-user_id}}',
            '{{%profiles}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-profiles-user_id}}',
            '{{%profiles}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-profiles-user_id}}',
            '{{%profiles}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-profiles-user_id}}',
            '{{%profiles}}'
        );

        $this->dropTable('{{%profiles}}');
    }
}
