<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * RegistrationForm is the model behind the registration form.
 *
 * @property-read User|null $user
 *
 */
class RegistrationForm extends Model
{
    public $email;
    public $password;
    public $repeatPassword;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'max' => 32, 'min' => 6],

            ['repeatPassword', 'required'],
            ['repeatPassword', 'string', 'max' => 32, 'min' => 6],
            [
                'repeatPassword',
                'compare',
                'compareAttribute' => 'password',
                'message' => 'Пароли не совпадают'
            ],

            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User'],
        ];
    }

    public function register()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = new User();
        $user->email = $this->email;
        $user->passwordHash($this->password);

        if (!$user->save()) {
            return false;
        }

        $profile = new Profile();
        $profile->user_id = $user->id;
        $profile->save();

        $token = new Token();
        $token->user_id = $user->id;
        $token->type = Token::CONFIRM;
        $token->generateToken(time() + 3600 * 24);
        $token->save();
    }
}