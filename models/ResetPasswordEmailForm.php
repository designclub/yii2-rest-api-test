<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ResetPasswordEmailForm is the model behind the login form.
 *
 * @property-read User|null $user
 *
 */
class ResetPasswordEmailForm extends Model
{
    public $email;

    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'checkUser'],
        ];
    }

    /**
     * Finds user by [[email]]
     */
    public function checkUser($attr)
    {
        $this->_user = User::findByUsername($this->{$attr});
        if (!$this->_user) {
            $this->addError($attr, 'Пользователя с таким email не существует.');
        }
    }

    public function accessChange()
    {
        if ($this->validate()) {
            $token = new Token();
            $token->user_id = $this->getUser()->id;
            $token->type = Token::CHANGE_PASSWORD;
            $token->generateToken(time() + 3600 * 24);
            return $token->save() ? $token : null;
        }

        return null;
    }
    
    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->email);
        }

        return $this->_user;
    }
}