<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tokens}}".
 *
 * @property int $id
 * @property string|null $token Токен
 * @property int|null $expire Дата окончания
 * @property int|null $type Тип токена
 * @property int|null $user_id
 *
 * @property User $user
 */
class Token extends \yii\db\ActiveRecord
{
    const AUTH = '1';
    const CONFIRM = '2';
    const CHANGE_PASSWORD = '3';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tokens}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['expire', 'type', 'user_id'], 'integer'],
            [['token'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Токен',
            'expire' => 'Дата окончания',
            'type' => 'Тип токена',
            'user_id' => 'User ID',
        ];
    }

    public function generateToken($expire)
    {
        $this->expire = $expire;
        $this->token = Yii::$app->security->generateRandomString();
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
