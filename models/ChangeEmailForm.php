<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ChangeEmailForm is the model behind the registration form.
 *
 * @property-read User|null $user
 *
 */
class ChangeEmailForm extends Model
{
    public $email;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'when' => function ($model) {
                return $model->email !== Yii::$app->user->getIdentity()->email;
            }],
            ['email', 'checkEmail'],
        ];
    }
}