<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%users}}".
 *
 * @property int $id
 * @property int|null $create_at Дата создания
 * @property int|null $update_at Дата обновления
 * @property int|null $last_visit Последний визит
 * @property string|null $email email
 * @property string|null $password Пароль
 * @property int|null $status Статус
 * @property int|null $role Роль
 *
 * @property Profile[] $profiles
 * @property Token[] $tokens
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['create_at', 'update_at', 'last_visit', 'status', 'role'], 'integer'],
            [['email', 'password'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_at' => 'Дата создания',
            'update_at' => 'Дата обновления',
            'last_visit' => 'Последний визит',
            'email' => 'email',
            'password' => 'Пароль',
            'status' => 'Статус',
            'role' => 'Роль',
        ];
    }

    public static function findByUsername($username)
    {
        return static::find()->where(['email' => $username])->one();
    }
    
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        $tokenModel = Token::find()
            ->where(['token' => $token, 'type' => Token::AUTH])
            ->andWhere(['>', 'expire', time()])
            ->one();

        if ($tokenModel === null) {
            return false;
        }

        return $tokenModel->user;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function passwordHash($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Gets query for [[Profile]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'id']);
    }
}
