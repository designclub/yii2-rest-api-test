<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ResetPasswordChangeForm is the model behind the login form.
 *
 * @property-read User|null $user
 *
 */
class ResetPasswordChangeForm extends Model
{
    public $token;
    public $oldPassword;
    public $newPassword;
    public $repeatPassword;

    private $_user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['token', 'oldPassword', 'newPassword', 'repeatPassword'], 'required'],
            ['token', 'validateToken'],
            ['oldPassword', 'validateOldPassword'],
            [
                'repeatPassword',
                'compare',
                'compareAttribute' => 'newPassword',
                'message' => 'Пароли не совпадают'
            ],
        ];
    }

    public function validateToken($attr)
    {
        $token = Token::findOne(['token' => $this->token]);
        if ($token) {
            $this->_user = User::findOne($token->user_id);
        } else {
            $this->addError($attr, 'Incorrect token.');
        }
    }

    public function validateOldPassword($attr)
    {
        $user = $this->getUser();
        if ($user && !$user->validatePassword($this->{$attr})) {
            $this->addError($attr, 'Incorrect password.');
        }
    }

    public function changePassword()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->getUser();
        $user->password = Yii::$app->security->generatePasswordHash($this->newPassword);
        if ($user->save()) {
            $token = new Token();
            $token->user_id = $user->id;
            $token->type = Token::AUTH;
            $token->generateToken(time() + 3600 * 24);
            $token->save();
            return $token;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $token = Token::findOne(['token' => $this->token]);
            $this->_user = User::findOne($token->user_id);
        }

        return $this->_user;
    }
}